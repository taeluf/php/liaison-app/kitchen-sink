<h1>Welcome to my Article</h1>
<p>You might be asking "Where is the kitchen sink?". Confused because you wanted to do some remodeling, but then you're looking at this silly page instead. Well, a "kitchen sink" page is used to display extremely common and highly-used html elements on a website. I don't know where the term comes from & I don't care. I heard it during a tutorial. So here we go.</p>

<p>The last paragraph has <b>no</b>(&lt;b&gt;) internal html elements. This one has <strong>several inline elements</strong>(&lt;strong&gt;)</p>

<h2>Paragraph containing &lt;q&gt;, &lt;abbr&gt;, and &lt;cite&gt;</h2>
<p>The last paragraph was a simple one with no internal html elements. This paragraph contains <q>quotation elements</q> (&lt;q&gt;), <abbr title="The Best Abbreviation Element">TBAE</abbr>(&lt;abbr title="..."&gt;), a cite element added by <cite>Reed Sutman</cite> (&lt;cite&gt;), a <bdo dir="rtl">bdo element</bdo> to do right-to-left text.
</p>

<h2>Blockquote no &lt;br&gt;</h2>
<blockquote cite=".">
This is a blockquote, which I am PRETTY sure is supposed to be it's own element & not in a &lt;p&gt;
This is the 2nd line of the blockquote.
And the third line.
But They all display on the same line.
</blockquote>

<h2>Blockquote with &lt;br&gt;</h2>
<blockquote cite=".">
This block quote contains &lt;br&gt; between lines <br>
So they are always separated, regardless of word-wrap & white-space styling <br>
It's up to you how you style your site.<br>
I will probably not require <br> inside my blockquotes, but idk!
</blockquote>


<hr>
<h2>Post Horizontal Rule</h2>
<p>Horizontal rules can be nice for organizing content. Let's have an image.</p>
<img src="../right-to-repair.jpg" alt="Right To Repair photo by Jackie Filson / Open Markets Institute" />
<small>Thank you <a href="https://twitter.com/JackieFilson">Jackie Filson</a> and <a href="https://www.openmarketsinstitute.org/">Open Markets Institute</a> For this image.</small>
<small>The Attribution & This text are each inside a &lt;small&gt;</small>
