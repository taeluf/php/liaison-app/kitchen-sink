<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# Kitchen Sink Liaison Addon  
Adds some pages to Liaison to help you define the base styles for your site.  
  
  
With Liaison, simply do:  
```  
\Lia\Addon\KitchenSink::enable($liaison);  
```  
  
Otherwise, see [code/KitchenSink.php](/code/KitchenSink.php) for more  
  
## Install  
```bash  
composer require taeluf/liaison.kitchen-sink v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/liaison.kitchen-sink": "v0.1.x-dev"}}  
```    
