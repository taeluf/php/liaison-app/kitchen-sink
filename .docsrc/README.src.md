# Kitchen Sink Liaison Addon
Adds some pages to Liaison to help you define the base styles for your site.


With Liaison, simply do:
```
\Lia\Addon\KitchenSink::enable($liaison);
```

Otherwise, see @see_file(code/KitchenSink.php) for more

## Install
@template(composer_install,taeluf/liaison.kitchen-sink)  
